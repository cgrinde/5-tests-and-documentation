# -*- coding: utf-8 -*-
"""Functions that perform calculations in the mytestdocs package

Author
------
Jenni Rinker
rink@dtu.dk
"""


def base10(x):
    """Take a given int and return 10 to the power of the int

    Arguments
    ---------
    x : int, float
        The exponent to which we will raise 10.

    Returns
    -------
    y : int, float
        10 ** x.
    """
    return 10 ** x


def double(x):
    """
    """
    return 3 * x


def lower_str(s):
    """Convert a string to lowercase

    Arguments
    ---------
    s : str
        The string to bring to lowercase.
    x : int, float
        The exponent to which we will raise 10.

    Returns
    -------
    s_out : int, float
        A lowercase version of the input string.
    """
    return s.lower()
