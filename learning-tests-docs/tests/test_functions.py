# -*- coding: utf-8 -*-
"""Tests for functions.py submodule

Author
------
Jenni Rinker
rink@dtu.dk
"""

from mytestdocs.functions import base10, double


def test_base10_value():
    """Verify that the base10 function returns the theoretical value
    """
    # given
    xs = [1, 3.4]
    ys_theo = [10, 2511.88643150958]
    # when
    ys = [base10(x) for x in xs]
    # then
    assert ys_theo == ys


def test_double_value():
    """Verify that the double function returns the theoretical value
    """
    # given
    x = 4
    y_theo = 8.
    # when
    y = double(x)
    # then
    assert y_theo == y
